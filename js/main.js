gsap.registerPlugin(ScrollTrigger);

const init = () => {
  // Scroll trigger
  // gsap.to('#intro img', {
  //   opacity: 0,
  //   scrollTrigger: {
  //     trigger: '#intro',
  //     start: 'top top',
  //     end: 'bottom center',
  //     scrub: true,
  //   },
  // });

  // Toggle Class
  gsap.set('#project02', {
    scrollTrigger: {
      trigger: '#project02',
      start: 'top bottom-=10%',
      end: 'bottom center-=10%',
      toggleClass: 'active',
    },
  });

  // Parallax Effect
  const parallaxTl = gsap.timeline({
    ease: 'none',
    scrollTrigger: {
      trigger: '.bcg-parallax',
      start: 'top bottom',
      scrub: true,
    },
  });

  parallaxTl
    .from('.bcg', {
      duration: 2,
      y: '-30%',
    })
    .from('.content-wrapper', {
      duration: .4,
      autoAlpha: 0,
    }, .4);

  // Pin effect
  gsap.to(['#intro h1', '#intro p'], {
    autoAlpha: 0,
    ease: 'none',
    scrollTrigger: {
      trigger: '#intro .content',
      start: 'top top+=5%',
      pin: true,
      scrub: true,
    },
  });

  // Toggle actions
  const projects = document.querySelectorAll('.project');

  projects.forEach(
    (project, index) => {
      gsap.from(project, {
        opacity: 0,
        yPercent: 5,
        scrollTrigger: {
          trigger: project.querySelector('img'),
          start: 'top bottom-=300',
          end: 'top center',
          // The values are bound to the scroll start/end
          // and its direction, in this order:
          // onEnter onLeave onEnterBack onLeaveBack
          toggleActions: 'play none none reverse',
          scrub: true,
          onUpdate: ({
            direction,
            isActive,
            progress,
            getVelocity
          }) => console.log(`Project0${index + 1}`, direction, progress, isActive, getVelocity()),
          onToggle: () => console.log(`Project0${index + 1}`, 'onToggle'),
          onEnter: () => console.log(`Project0${index + 1}`, 'onEnter'),
          onLeave: () => console.log(`Project0${index + 1}`, 'onLeave'),
          onEnterBack: () => console.log(`Project0${index + 1}`, 'onEnterBack'),
          onLeaveBack: () => console.log(`Project0${index + 1}`, 'onLeaveBack'),
          markers: true,
        },
      });
    },
  );
}

window.addEventListener('load', () => {
  init();
});
